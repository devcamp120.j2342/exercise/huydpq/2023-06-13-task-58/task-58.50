package com.devcamp.employeelisthibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeListHibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeListHibernateApplication.class, args);
	}

}
