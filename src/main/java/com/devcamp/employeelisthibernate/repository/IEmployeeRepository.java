package com.devcamp.employeelisthibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.employeelisthibernate.model.CEmployee;

public interface IEmployeeRepository extends JpaRepository<CEmployee, Long> {
    
}
