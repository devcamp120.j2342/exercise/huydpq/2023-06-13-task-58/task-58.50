package com.devcamp.employeelisthibernate.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.employeelisthibernate.model.CEmployee;
import com.devcamp.employeelisthibernate.repository.IEmployeeRepository;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CEmployeeController {
    @Autowired
    IEmployeeRepository pEmployeeRepository;

    @GetMapping("/employees")
    public ResponseEntity<List<CEmployee>> getAllEmployees(){
        try {
            List<CEmployee> listEmployee = new ArrayList<CEmployee>();
            pEmployeeRepository.findAll().forEach(listEmployee::add);
            return new ResponseEntity<>(listEmployee, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
